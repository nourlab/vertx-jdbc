package org.szhao.jdbc;

import io.vertx.core.*;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.jdbc.JDBCClient;
import io.vertx.ext.sql.SQLClient;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MainVerticle extends AbstractVerticle {
  private Logger LOG = LoggerFactory.getLogger(MainVerticle.class);

  private SQLClient client;

  @Override
  public void init(Vertx vertx, Context context) {
    super.init(vertx, context);

    if (config() == null || config().isEmpty()) {
      client = JDBCClient.createShared(vertx, new JsonObject().put("url", "jdbc:mariadb://localhost:3306/security")
        .put("driver_class", "org.mariadb.jdbc.Driver")
        .put("max_pool_size", 30)
        .put("user", "root")
        .put("password", "root"));
    } else {
      client = JDBCClient.createShared(vertx, config().getJsonObject("db"));
    }


  }
  @Override
  public void start(Promise<Void> startPromise) throws Exception {

    vertx.deployVerticle(new AccountServiceVerticle(), new DeploymentOptions().setConfig(config()));


    final Router router = Router.router(vertx);

    router.route(HttpMethod.POST, "/accounts").handler(this::createAccounts);
    router.route(HttpMethod.GET, "/accounts").handler(this::findAccounts);

    vertx.createHttpServer()
      .requestHandler(router)
      .listen(config().getInteger("http.port", 8888), http -> {
        if (http.succeeded()) {
          startPromise.complete();
          LOG.info("HTTP server started on port 8888");
        } else {
          startPromise.fail(http.cause());
        }
      });
  }

  private void findAccounts(RoutingContext context) {

    context.request().bodyHandler(buf -> {
      MultiMap params = context.request().params();
      LOG.info("get accounts: criteria {}", params);

      vertx.eventBus().request(AccountServiceVerticle.FIND_ACCOUNTS, params.toString(), ar -> {
        LOG.info("find accounts response: {}", ar.result().body());
        context.response().setStatusCode(200).end(ar.result().body().toString());
      });

    });

  }

  private void createAccounts(RoutingContext context) {
    context.request().bodyHandler(buf -> {
      JsonArray accounts = buf.toJsonArray();

      vertx.eventBus().request(AccountServiceVerticle.CREATE_ACCOUNTS, accounts, ar -> {
        LOG.info("create accounts response: {}", ar.result().body());
        context.response().setStatusCode(200).end(ar.result().body().toString());
      });

    });
  }


}
