package org.szhao.jdbc;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.jdbc.JDBCClient;
import io.vertx.ext.sql.ResultSet;
import io.vertx.ext.sql.SQLClient;
import io.vertx.ext.sql.SQLConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.stream.Collectors;

public class AccountServiceVerticle extends AbstractVerticle {

  private static final Logger LOG = LoggerFactory.getLogger(AccountServiceVerticle.class);

  public static final String FIND_ACCOUNTS = "accounts::find";
  public static final String DELETE_ACCOUNTS = "accounts::delete";
  public static final String CREATE_ACCOUNTS = "accounts::create";

  public static final String CREATE_ACCOUNTS_SQL = "INSERT INTO account (id, username) VALUES (?, ?)";

  private SQLClient client;

  @Override
  public void start() {

    client = JDBCClient.createShared(vertx, new JsonObject());

    vertx.eventBus().consumer(FIND_ACCOUNTS).handler(m -> {
      LOG.info("find user: criteria {}", m.body());
      client.getConnection(ar ->{
        if (ar.succeeded()) {
          SQLConnection conn = ar.result();
          conn.query("SELECT * FROM account", res -> {
            if (ar.succeeded()) {
              ResultSet rs = res.result();

              JsonArray accounts = rs.getRows().stream()
                .reduce(new JsonArray(), JsonArray::add, JsonArray::addAll);

              m.reply(accounts);
            }
          });
        }
      });
    });


    vertx.eventBus().consumer(DELETE_ACCOUNTS).handler(m -> {
      LOG.info("delete user: criteria {}", m.body());

      m.reply(new JsonObject().put("id", "deleted-account-123-mpr"));
    });

    vertx.eventBus().consumer(CREATE_ACCOUNTS).handler(m -> {
      if (! (m.body() instanceof JsonArray)) {
        throw new RuntimeException();
      }

      JsonArray body = (JsonArray) m.body();
      LOG.info("create user: criteria {}", body);
      client.getConnection(ar -> {
        if (ar.succeeded()) {
          SQLConnection conn = ar.result();

          List<JsonArray> batch = body.stream().map(obj -> (JsonObject) obj).map(obj -> new JsonArray()
            .add(obj.getString("id"))
            .add(obj.getString("username"))).collect(Collectors.toList());

          conn.batchWithParams(CREATE_ACCOUNTS_SQL, batch, res -> {
            m.reply(new JsonObject().put("created", res.result()));
          });
        }
      });
    });
  }
}
